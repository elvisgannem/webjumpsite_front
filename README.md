# Instruções para rodar o projeto

Como foram enviados os arquivos para a API e para ser executados no servidor local, os coloquei em outro repositório para não ficar todo
misturado, é necessário ir até o repositório da API: https://bitbucket.org/elvisgannem/webjumpsite_back/src/main/ baixar esses arquivos, e
executar: 

npm install

npm start

Depois, baixe os arquivos deste repositório: https://bitbucket.org/elvisgannem/webjumpsite_front/src/main/ e execute:

npm install ou yarn install

e depois: npm start ou yarn start

# Este projeto foi feito usando React.js, Styled Components e fetch para as chamadas para a API.